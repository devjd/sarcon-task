var express = require('express');
var app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http,{
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
}
});
var mysql = require('mysql');

// TO PREVENT CORS ERRORS WHILE CALLING SERVER CALLS
app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

// MYSQL DATABASE CONNECTION
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "sarcon_events"
});
con.connect(function(err) {
    if (err) throw err;
});

// API TO GET ALL EVENTS
app.post('/get-events', function(req, res){
    con.query("SELECT * FROM events", function (err, result, fields) {
        if (err) throw err;
        let output = {
            code: 1,
            data: result
        };
        res.send(output);
    });
});

// SOCKET CONNECTION AND EVENTS HANDLLED IN IT
io.on('connection', function(socket) {
    socket.on('status_changed', function (data) {
        var sql = "UPDATE events SET status = IF(status = 'enable', 'disable', 'enable') WHERE id = " + data.id;
        con.query(sql, function (err, result) {
            if (err) throw err;
            con.query("SELECT * FROM events WHERE id = " + data.id, function (err2, result2, fields) {
                if (err2) throw err2;
                let output = {
                    code: 1,
                    data: result2,
                };
                io.emit('eventChanged', output);
            });
        });
        
    });
});

http.listen(3000);
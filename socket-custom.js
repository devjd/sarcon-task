var socket = io("http://localhost:3000");
socket.on('eventChanged', function(data) {
    let isDisabled = (data.data[0].status=='disable')?true:false;
    // ADMIN SIDE CHANGES
    $.each($(document).find('#datatablesSimple tbody tr'),function(){
        if($(this).attr('data-id') == data.data[0].id){
            if(isDisabled){
                $(this).find('.js-event-status-btn').removeClass('btn-success').removeClass('btn-danger').addClass('btn-success').text('Enable this event').prop('disabled',false);
            }else{
                $(this).find('.js-event-status-btn').removeClass('btn-success').removeClass('btn-danger').addClass('btn-danger').text('Disable this event').prop('disabled',false);
            }
        }
    });
    // USER SIDE CHANGES
    $(document).find('#event_card_' + data.data[0].id).find('.js-event-card-join-btn').prop('disabled',isDisabled);
    if(isDisabled){
        $(document).find('#event_card_' + data.data[0].id).find('.js-event-card-join-btn').text('Event closed!');
    }else{
        $(document).find('#event_card_' + data.data[0].id).find('.js-event-card-join-btn').text('Join Now!');
    }
});
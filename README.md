### Please note
- I have developed this task in local and not hosted in live.
- If you want me to upload to live then sure i can do it as well. 

### Start-up Guide
- Find 'sarcon_events.sql' database from database folder
- Fire 'node index.js' to start the server'
- Then you can load both the index pages from index.html and admin/index.html

### Task Information 
- Set up basic UI for both User/Admin side pages 
- Get and load the event data from database 
- To demonstrate the use of socket mainly


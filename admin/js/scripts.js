/*!
    * Start Bootstrap - SB Admin v7.0.4 (https://startbootstrap.com/template/sb-admin)
    * Copyright 2013-2021 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-sb-admin/blob/master/LICENSE)
    */
    // 
// Scripts
// 

window.addEventListener('DOMContentLoaded', event => {

    // Toggle the side navigation
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
        // Uncomment Below to persist sidebar toggle between refreshes
        // if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
        //     document.body.classList.toggle('sb-sidenav-toggled');
        // }
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }

});

$(document).ready(function() {
    // FUNCTION TO GET AND SET EVENTS AT ADMIN SIDE WEBPAGE
    async function getEvents() {
        let request;
        request = await $.ajax({
            type: 'POST',
            url: "http://localhost:3000/get-events",
            data: {},
            datatype: 'json',
        });
        if(request){
            let result = request;//JSON.parse(request);
            if(result.code == 1){
                $(document).find('#datatablesSimple tbody').empty();
                let cardHtml = '<tr><td class="js-event-name"></td><td class="js-event-description"></td><td><button class="btn btn-block js-event-status-btn"></button></td></tr>';
                result.data.forEach((element,key) => {
                    if($(document).find('#datatablesSimple tbody tr').length){
                        $(document).find('#datatablesSimple tbody tr').last().after(cardHtml);
                    }else{
                        $(document).find('#datatablesSimple tbody').append(cardHtml);
                    }
                    let lastWrapper = $(document).find('#datatablesSimple tbody tr').last();
                    lastWrapper.attr('data-id',element.id);
                    lastWrapper.find('.js-event-name').text(element.name);
                    lastWrapper.find('.js-event-description').text(element.description);
                    let isDisabled = (element.status=='disable')?true:false;
                    if(isDisabled){
                        lastWrapper.find('.js-event-status-btn').addClass('btn-success').text('Enable this event');
                    }else{
                        lastWrapper.find('.js-event-status-btn').addClass('btn-danger').text('Disable this event');
                    }
                    
                });
            }
            console.log(result)
        }
    };
    getEvents();

    // TO PERFORM EVENT ENABLE/DISABLE FROM EVENT LISTING
    $(document).on('click','.js-event-status-btn',async function(){
        var that = $(this);
        that.text('Processing...');
        that.prop('disabled',true);
        data = {
            id: that.closest('tr').attr('data-id')
        }
        await socket.emit('status_changed', data);
    });
});

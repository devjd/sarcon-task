$(document).ready(function() {
    // FUNCTION TO GET AND SET EVENTS AT CLIENT SIDE WEBPAGE
    async function getEvents() {
        let request;
        request = await $.ajax({
            type: 'POST',
            url: "http://localhost:3000/get-events",
            data: {},
            datatype: 'json',
        });
        if(request){
            let result = request;
            if(result.code == 1){
                $(document).find('.js-events-wrapper').empty();
                let cardHtml = '<div class="col mb-5 js-event-card"><div class="card h-100"><div class="card-body p-4"><div class="text-center"><h5 class="fw-bolder js-event-card-name"></h5><span class="js-event-card-description"></span></div></div><div class="card-footer p-4 pt-0 border-top-0 bg-transparent"><div class="text-center"><button class="btn btn-outline-dark mt-auto js-event-card-join-btn">Join Now!</button></div></div></div></div>';
                result.data.forEach((element,key) => {
                    if($(document).find('.js-event-card').length){
                        $(document).find('.js-event-card').last().after(cardHtml);
                    }else{
                        $(document).find('.js-events-wrapper').append(cardHtml);
                    }
                    let lastWrapper = $(document).find('.js-event-card').last();
                    lastWrapper.attr('id','event_card_' + element.id);
                    lastWrapper.find('.js-event-card-name').text(element.name);
                    lastWrapper.find('.js-event-card-description').text(element.description);
                    let isDisabled = (element.status=='disable')?true:false;
                    lastWrapper.find('.js-event-card-join-btn').prop('disabled',isDisabled);
                    if(isDisabled){
                        lastWrapper.find('.js-event-card-join-btn').text('Event closed!');
                    }
                });
            }
            console.log(result)
        }
    };
    getEvents();
});